import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Item} from "./item";

@Injectable({
  providedIn: 'root'
})
export class FilterItemService {

  private dataSubject = new BehaviorSubject<string>('');

  $criterion = this.dataSubject.asObservable();

  constructor() { }

  addCriterion(criterion: string):void {
    this.dataSubject.next(criterion);
  }
}
