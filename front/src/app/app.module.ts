import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ChartsModule} from "ng2-charts";
import {CookieService} from 'ngx-cookie-service';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {LayoutModule} from '@angular/cdk/layout';

import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBottomSheetModule} from "@angular/material/bottom-sheet";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSortModule} from "@angular/material/sort";
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from '@angular/material/toolbar';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CookieSheetComponent} from './cookie-sheet/cookie-sheet.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DefinitionsComponent} from './definitions/definitions.component';
import {HomeComponent} from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';

import {ItemDataService} from "./item-data.service";
import {FilterItemService} from "./filter-item.service";
import {PieChartComponent} from './pie-chart/pie-chart.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    CookieSheetComponent,
    PrivacyPolicyComponent,
    DashboardComponent,
    DefinitionsComponent,
    PieChartComponent,
    DetailsComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    ChartsModule,
    FormsModule,
    LayoutModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatListModule,
    MatFormFieldModule,
    MatGridListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    ReactiveFormsModule,
  ],
  providers: [CookieService, ItemDataService, FilterItemService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
