import {AfterViewChecked, Component, OnInit, ViewChild} from '@angular/core';
import {Item} from "../item";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {ItemDataService} from "../item-data.service";
import {FilterItemService} from "../filter-item.service";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {Observable} from "rxjs";
import {map, shareReplay} from "rxjs/operators";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewChecked, OnInit {

  //isSmallScreen: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.XSmall]).pipe(map(result => result.matches), shareReplay());
  items: Item[] = this.itemDataService.items;

  // displayedColumns: string[] = ['name', 'type', 'pie', 'calories', 'fat', 'carbohydrates', 'fiber', 'protein'];
  displayedColumns: string[] = ['name', 'type', 'calories', 'pie'];
  dataSource = new MatTableDataSource<Item>(this.items);

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  @ViewChild(MatSort) sort: MatSort | undefined;

  constructor(private breakpointObserver: BreakpointObserver, private itemDataService: ItemDataService, private filterItemService: FilterItemService) {
  }

  ngAfterViewChecked(): void {
    // @ts-ignore
    this.dataSource.sort = this.sort;
    // @ts-ignore
    this.dataSource.paginator = this.paginator;
    // this.itemDataService.$items.subscribe(res => {
    //   this.dataSource.data = res as Item[];
    // });
    }

  ngOnInit(): void {
    this.filterItemService.$criterion.subscribe(criterion => {
      this.dataSource.filter = criterion.trim().toLocaleLowerCase();
    });
    // this.itemDataService.$items.subscribe(items => {
    //   this.itemDataService.items = items;
    // });
  }

  public filterItems = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public getAllItems = () => {
    // this.itemDataService.$items.subscribe(res => {
    //   this.dataSource.data = res as Item[];
    // });

    // this.itemDataService.getData('api/item').subscribe(res => {
    //     this.dataSource.data = res as Item[];
    //   })
  }

  public redirectToDetails = (id: string) => {

  }
}
