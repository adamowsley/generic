//item quantities are normalized to 1 gram
export interface Item {
  name: string;
  type: string;          //fruit, vegetable, legume, fish
  pie: number[];
  calories: number;      //kcal
  fat: number;           //g
  sodium: number;        //mg
  potassium: number;     //mg
  carbohydrates: number; //g
  fiber: number;         //g
  sugar: number;         //g
  protein: number;       //g
  checked: boolean;
}
