import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CookieSheetComponent } from './cookie-sheet.component';

describe('CookieSheetComponent', () => {
  let component: CookieSheetComponent;
  let fixture: ComponentFixture<CookieSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CookieSheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CookieSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
