import {Component, OnInit} from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MatButton} from "@angular/material/button";
import {MatListModule} from '@angular/material/list';
import {BreakpointObserver} from "@angular/cdk/layout";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-cookie-sheet',
  templateUrl: './cookie-sheet.component.html',
  styleUrls: ['./cookie-sheet.component.scss']
})
export class CookieSheetComponent implements OnInit {

  constructor(private bottomSheetRef: MatBottomSheetRef<CookieSheetComponent>, private cookieService: CookieService) {
  }

  ngOnInit(): void {
  }

  agree(event: MouseEvent): void {
    this.cookieService.set('agree', 'true');
    console.log(this.cookieService.get('agree'));
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
