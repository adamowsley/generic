import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Observer, of} from "rxjs";
import {Item} from "./item";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ItemDataService {

  items: Item[] = [
    {name: 'Apple', type: 'fruit', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Apricot', type: 'fruit', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Artichoke', type: 'fruit', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Arugula', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Asparagus', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Avocado', type: 'fruit', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Banana', type: 'fruit', pie: [50,25,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Bass', type: 'fish', pie: [10,25,25,40], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Blackberry', type: 'fruit', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Blueberry', type: 'fruit', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Carrot', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Celery', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Cherry', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Chickpea', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Date', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Eggplant', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Ginger', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Lentil', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Navy Bean', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Onion', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Peanut', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Potato', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Sweet Potato', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Tomato', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
    {name: 'Walnut', type: 'vegetable', pie: [5,50,20,25], calories: 0.21505, fat: 0, sodium: 0, potassium: 2.47312, carbohydrates: 0.43011, fiber: 0.02151, sugar: 0.01251, protein: 0.02151, checked: false},
  ];

  private dataSubject = new BehaviorSubject<Item[]>(this.items);

  $items = this.dataSubject.asObservable();

  constructor(private http: HttpClient) {
    console.log('Production: ' + environment.production);
    console.log('URL: ' + environment.urlAddress);
  }

  addItem(item: Item):void {
    this.items.push(item);
    this.dataSubject.next(this.items);
  }

  private createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${route}`;
  }

  public getData = (route: string) => {
    return this.items;
   // return this.dataSubject.next(this.items);
   //  return this.$items;
   //  return new Observable((observer: Observer<any>) => {
   //    observer.next(this.items);
   //    observer.complete();
   //  });
    // return this.http.get(this.createCompleteRoute(route, environment.urlAddress));
  }
}
