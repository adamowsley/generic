import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PrivacyPolicyComponent} from "./privacy-policy/privacy-policy.component";
import {NavigationBehaviorOptions} from "@angular/router";
import {DefinitionsComponent} from "./definitions/definitions.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  // {path: 'item', loadChildren: () => import ('./item.module').then(m => m.ItemModule)},
  {path: 'definitions', component: DefinitionsComponent},
  {path: 'privacy-policy', component: PrivacyPolicyComponent},
  {path: '**', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor() {
  }
}
